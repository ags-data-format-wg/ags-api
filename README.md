# AGS API


## Description

This is a research and development project to determine the options/problems/opportunities of creating and consuming rest API payloads of geotechnical data, using the AGS4 data format, or more specifically the AGS4 data dictionary.

The outcome of the project will be a framework and guidance for developers who wish to create AGS-compliant rest APIs for geotechnical data and developers and users who wish to consume them.

## In scope

- routes
- payload schemas
- filters
- schema validation

## Out of scope

- anything other than 'GET' requests.
- guidance on creatiion of the functions that generate the APIs
- guidance on creation of the functions that use APIs


Although guidance will not be given, testing of the concepts will provide examples.

## Standards and software typically used

- AGS 4.1.1
- Open API
- Sqlite
- Postman
- Flask
- Python
    - fastapi
    - sqlAlchemy
    - streamlit
    - plotly

## Use Cases

These use cases will be used as examples to develop payloads and filters
They are intended to reflect real-world use of geotechnical data and provide a suitable range os scenarios.
They need to be prioritised.

1. Project details such as name, client, contractor
2. A list of all exploratory holes with some useful data such as hole type, co-ordinates and final depth.
3. A list of all samples with hole reference, sample type and sample depth.
4. A list of all SPT test results with hole reference and depth.
5. A list of all water content tests with hole reference depth and geology.
6. All data relating to a single hole (excluding lab test data).
7. All data relating to a single hole.
8. A list of all exploratory holes within an enclosed polygon.
9. A list of all SPT tests results within an enclosed polygon.
10. All data relating to a single test.
11. Water level over time (monitoring data).

## Emerging issues/ideas

1. Large data sets e.g. advanced testing may be best handled by including pointers to the data (FSET).
2. Need to explore pagination - does it need to be specified or do we leave it up to the API designer?

### Payload schemas

1. Use json data types or strings? Data types preferred for ease of use. 
2. Provide elevation and depth data as standard.
3. Provide GEOL data for test data.

## Support

Please use the isues for all help requests, bugs and thoughts.

## Roadmap

in progress

## Contributing

Please contact us via the issues to let us know if you can help?

## Authors and acknowledgment

TBC

## Licence

The licence is the LGPL3
